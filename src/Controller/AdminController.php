<?php

namespace App\Controller;

use App\Entity\Cause;
use App\Form\CauseFormType;
use App\Form\NewsletterFormType;
use App\Repository\CauseRepository;
use App\Repository\DonateurRepository;
use App\Repository\DonRepository;
use App\Repository\NewsletterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin')]
#[IsGranted("ROLE_ADMIN")]
class AdminController extends AbstractController
{
    private $entityManager;
    private $causeRepository;
    private $donRepository;
    private $newsletterRepository;
    private $donateursRepository;

    public function __construct(EntityManagerInterface $entityManager, CauseRepository $causeRepository, DonRepository $donRepository, NewsletterRepository $newsletterRepository, DonateurRepository $donateurRepository)
    {
        $this->entityManager = $entityManager;
        $this->causeRepository = $causeRepository;
        $this->donRepository = $donRepository;
        $this->newsletterRepository = $newsletterRepository;
        $this->donateursRepository = $donateurRepository;
    }

    #[Route('', name: 'app_admin')]    
    /**
     * index
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_admin');
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'newsletterForm' => $newsletterForm->createView(),
        ]);
    }

    #[Route('/causes', name:"app_admin_causes")]
    public function causes(Request $request, MailerInterface $mailer){
        $count = count($this->causeRepository->findAll());
        $nombre_pages = $count % 3 == 0 ? intdiv($count, 3) : intdiv($count, 3) + 1;

        $page_actuel = isset($_GET['page']) ? $_GET['page'] : 1;

        $causesSet = $this->causeRepository->findBy([], [], 3, 3*($page_actuel -1));

        $causes = [];

        foreach ($causesSet as $cause) {
            $total = 0;
            foreach ($cause->getDons() as $don) {
                if ($don->getStatut() == "completed") $total += $don->getMontant();
            }

            $pourcentage = round(($total /$cause->getObjectifCause()), 2);

            $causes[] = [
                'id_cause' => $cause->getIdCause(),
                'nom_cause' => $cause->getNomCause(),
                'description_cause' => $cause->getDescripionCause(),
                'objectif_cause' => $cause->getObjectifCause(),
                'total_cause' => $total,
                'image_cause' => $cause->getImageCause(),
                'pourcentage' => $pourcentage
            ];
        }

        $newCause = new Cause();

        $causeForm = $this->createForm(CauseFormType::class ,$newCause);
        $causeForm->handleRequest($request);
        
        if ($causeForm->isSubmitted() && $causeForm->isValid()) { 
            $newCause->setNomCause($causeForm->get('nom_cause')->getData());
            $newCause->setDescripionCause($causeForm->get('descripion_cause')->getData());
            $newCause->setObjectifCause($causeForm->get('objectif_cause')->getData());

            $illlustration = $causeForm->get('image_cause')->getData();

            if ($illlustration) {
                $image_cause = uniqid() . '.' . $illlustration->guessExtension();

                try {
                    $illlustration->move($this->getParameter('kernel.project_dir'). '/public/img/causes/' , $image_cause);
                } catch (FileException $e) {
                    return new Response($e->getMessage());
                }

                $newCause->setImageCause('/img/causes/' . $image_cause);
            }

            $newCause->setArchive(false);
            $newCause->setIdCause(uniqid());

            $this->entityManager->persist($newCause);
            $this->entityManager->flush();

            foreach ($this->newsletterRepository->findAll() as $newsletter) {
                $email = (new TemplatedEmail())
                    ->from(new Address('songre226@gmail.com', 'Songre'))
                    ->to($newsletter->getEmail())
                    ->subject("Une nouvelle cause vient d'être ajoutée. Soyez le premier à y faire un don !")
                    ->htmlTemplate('admin/newsletterEmail.html.twig')
                    ->context([
                        'cause' => [
                            'id_cause' => $newCause->getIdCause(),
                            'image_cause' => $newCause->getImageCause(),
                            'nom_cause' => $newCause->getNomCause(),
                            'description_cause' => $newCause->getDescripionCause()
                        ],
                    ]);

                $mailer->send($email);
            }   

            return $this->redirectToRoute("app_admin_cause_details", ['id_cause' => $newCause->getIdCause()]);
        }

        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_admin_causes');
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render("admin/causes.html.twig", [
            'causes' => $causes,
            'causeForm' => $causeForm->createView(),
            'total' => $count,
            'statut_page' => [
                'page_actuel' => $page_actuel,
                'nbre_pages' => $nombre_pages
            ],
            'newsletterForm' => $newsletterForm->createView(),
        ]);
    }

    #[Route('/cause/{id_cause}', name:"app_admin_cause_details")]
    public function detailsCause(Request $request, string $id_cause){
        $cause = $this->causeRepository->findOneBy(['id_cause' => $id_cause]);
        $total = 0;
        foreach ($cause->getDons() as $don) {
            if ($don->getStatut() == "completed") $total += $don->getMontant();
        }

        $donsSet = $this->donRepository->findBy(['statut' => 'completed', 'cause' => $cause], ['date_heure' => "DESC"]);
        $dons = [];

        foreach ($donsSet as $don) {
            $dons[] = [
                'id_don' => $don->getIdDon(),
                'date_heure' => $don->getDateHeure(),
                'montant' => $don->getMontant(),
                'id_transaction' => $don->getIdTransaction(),
                'donateur' => $don->getDonateur()->getPrenomDonateur() . " " . $don->getDonateur()->getNomDonateur()
            ];
        }

        $pourcentage = round(($total /$cause->getObjectifCause()), 2);

        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_admin_cause_details', ['id_cause' => $id_cause]);
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render("admin/detailsCause.html.twig", [
            'cause' => [
                'id_cause' => $cause->getIdCause(),
                'nom_cause' => $cause->getNomCause(),
                'description_cause' => $cause->getDescripionCause(),
                'objectif_cause' => $cause->getObjectifCause(),
                'image_cause' => $cause->getImageCause(),
                'recolte_cause' => $total,
                'pourcentage' => $pourcentage,
                'archive' => $cause->isArchive()
            ],
            'dons' => $dons,
            'newsletterForm' => $newsletterForm->createView(),
        ]);
    }

    #[Route('/cause/{id_cause}/archiver', name:"app_admin_cause_archiver")]
    public function archiverCause(string $id_cause){
        $cause = $this->causeRepository->findOneBy(['id_cause' => $id_cause]);
        $cause->setArchive(true);

        $this->entityManager->flush();

        return $this->redirectToRoute("app_admin_cause_details", ['id_cause' => $id_cause]);
    }

    #[Route('/cause/{id_cause}/desarchiver', name:"app_admin_cause_desarchiver")]
    public function desarchiverCause(string $id_cause){
        $cause = $this->causeRepository->findOneBy(['id_cause' => $id_cause]);
        $cause->setArchive(false);

        $this->entityManager->flush();

        return $this->redirectToRoute("app_admin_cause_details", ['id_cause' => $id_cause]);
    }

    #[Route('/donateurs', name:"app_admin_donateurs")]
    public function donateurs(Request $request){
        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_admin_causes');
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        $count = count($this->donateursRepository->findAll());
        $nombre_pages = $count % 10 == 0 ? intdiv($count, 10) : intdiv($count, 10) + 1;
        
        $page_actuel = isset($_GET['page']) ? $_GET['page'] : 1;

        $donateursSet = $this->donateursRepository->findBy(['isVerified' => 1], null, 10, 10*($page_actuel -1));

        $donateurs = [];

        foreach ($donateursSet as $donateur) {
            $total = 0;
            $contri = 0;

            foreach ($donateur->getDons() as $don) {
                if ($don->getStatut() == "completed"){
                    $total += $don->getMontant();
                    $contri += 1;
                } 
            }

            $donateurs[] = [
                'id_donateur' => $donateur->getIdDonateur(),
                'nom_donateur' => $donateur->getNomDonateur(),
                'prenom_donateur' => $donateur->getPrenomDonateur(),
                'email_donateur' => $donateur->getEmail(),
                'contact_donateur' => $donateur->getContactDonateur(),
                'contribution_donateur' => $contri,
                'total_donateur' => $total,
            ];
        }

        return $this->render("admin/donateurs.html.twig", [
            'newsletterForm' => $newsletterForm,
            'donateurs' => $donateurs,
            'statut_page' => [
                'page_actuel' => $page_actuel,
                'nbre_pages' => $nombre_pages
            ],
        ]);
    }
}
