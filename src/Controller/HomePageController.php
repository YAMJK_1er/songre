<?php

namespace App\Controller;

use App\Entity\Don;
use App\Form\ContactFormType;
use App\Form\DonateurFormType;
use App\Form\DonFormType;
use App\Form\NewsletterFormType;
use App\Repository\CauseRepository;
use App\Repository\DonateurRepository;
use App\Repository\DonRepository;
use App\Repository\NewsletterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Notifier\TexterInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    private $entityManager;
    private $causeRepository;
    private $donRepository;
    private $donateurRepository;
    private $newsletterRepository;

    public function __construct(EntityManagerInterface $entityManager, CauseRepository $causeRepository, DonRepository $donRepository, DonateurRepository $donateurRepository, NewsletterRepository $newsletterRepository)
    {
        $this->entityManager = $entityManager;
        $this->causeRepository = $causeRepository;
        $this->donRepository = $donRepository;
        $this->donateurRepository = $donateurRepository;
        $this->newsletterRepository = $newsletterRepository;
    }

    #[Route('', name: 'app_home_page')]
    public function index(Request $request, TexterInterface $texter): Response
    {
        $count = count($this->causeRepository->findBy(['archive' => false]));
        $nombre_pages = $count % 3 == 0 ? intdiv($count, 3) : intdiv($count, 3) + 1;
        
        $page_actuel = isset($_GET['page']) ? $_GET['page'] : 1;
        
        $causesSet = $this->causeRepository->findBy(['archive' => false], [], 3, 3*($page_actuel -1));
        
        $causes = [];

        foreach ($causesSet as $cause) {
            $total = 0;
            foreach ($cause->getDons() as $don) {
                if ($don->getStatut() == "completed") $total += $don->getMontant();
            }

            $pourcentage = round(($total /$cause->getObjectifCause()), 2);

            $causes[] = [
                'id_cause' => $cause->getIdCause(),
                'nom_cause' => $cause->getNomCause(),
                'description_cause' => $cause->getDescripionCause(),
                'objectif_cause' => $cause->getObjectifCause(),
                'total_cause' => $total,
                'image_cause' => $cause->getImageCause(),
                'pourcentage' => $pourcentage
            ];
        }

        $nbre_donateurs = count($this->donateurRepository->findAll());
        
        $objectifTotal = 0;
        $recolteTotale = 0;

        foreach ($this->causeRepository->findAll() as $cause) {
            $objectifTotal += $cause->getObjectifCause();
        }

        foreach ($this->donRepository->findBy(['statut' => 'completed']) as $don) {
            $recolteTotale += $don->getMontant();
        }
        
        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();
                
                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();
                
                return $this->redirectToRoute('app_home_page');
            }
        }
        
        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        $contactForm = $this->createForm(ContactFormType::class, null, [
            'action' => $this->generateUrl('app_handle_contact')
        ]);

        return $this->render('home_page/index.html.twig', [
            'causes' => $causes,
            'causesTotales' => $count,
            'nbreDonateurs' => $nbre_donateurs,
            'objectifTotale' => $objectifTotal,
            'recolteTotale' => $recolteTotale,
            'statut_page' => [
                'page_actuel' => $page_actuel,
                'nbre_pages' => $nombre_pages
            ],
            'newsletterForm' => $newsletterForm->createView(),
            'contactForm' => $contactForm->createView()
        ]);
    }

    #[Route('/cause/{id_cause}', name:"app_cause_details")]
    public function detailsCause(Request $request, string $id_cause){
        $cause = $this->causeRepository->findOneBy(['id_cause' => $id_cause]);

        $total = 0;
        foreach ($cause->getDons() as $don) {
            if ($don->getStatut() == "completed") $total += $don->getMontant();
        }

        $pourcentage = round(($total /$cause->getObjectifCause()), 2);

        $newDon = new Don();

        $donForm = $this->createForm(DonFormType::class, $newDon, [
            'action' => $this->generateUrl('app_cause_donner', ['id_cause' => $id_cause])
        ]);

        $donsSet = $this->donRepository->findBy(['cause' => $cause, 'statut' => "completed"], ['date_heure' => "DESC"], 10);

        $donsRecents = [];

        foreach ($donsSet as $don){
            $donsRecents[] = [
                'donateur' => $don->getDonateur()->getPrenomDonateur() . " " . $don->getDonateur()->getNomDonateur(),
                'montant' => $don->getMontant(),
                'date_heure' => $don->getDateHeure()
            ];
        }

        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_cause_details', ['id_cause' => $id_cause]);
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render("home_page/detailsCause.html.twig", [
            'cause' => [
                'id_cause' => $cause->getIdCause(),
                'nom_cause' => $cause->getNomCause(),
                'description_cause' => $cause->getDescripionCause(),
                'objectif_cause' => $cause->getObjectifCause(),
                'image_cause' => $cause->getImageCause(),
                'recolte_cause' => $total,
                'pourcentage' => $pourcentage
            ],
            'donForm' => $donForm->createView(),
            'donsRecents' => $donsRecents,
            'newsletterForm' => $newsletterForm->createView(),
        ]);
    }

    #[Route('infos', name:"app_infos")]
    public function infos(Request $request){
        $count = count($this->causeRepository->findBy(['archive' => false]));
        $nbre_donateurs = count($this->donateurRepository->findAll());
        $objectifTotal = 0;
        $recolteTotale = 0;

        foreach ($this->causeRepository->findAll() as $cause) {
            $objectifTotal += $cause->getObjectifCause();
        }

        foreach ($this->donRepository->findBy(['statut' => 'completed']) as $don) {
            $recolteTotale += $don->getMontant();
        }

        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_infos');
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render("home_page/infos.html.twig", [
            'causesTotales' => $count,
            'nbreDonateurs' => $nbre_donateurs,
            'objectifTotale' => $objectifTotal,
            'recolteTotale' => $recolteTotale,
            'newsletterForm' => $newsletterForm->createView(),
        ]);
    }

    #[Route('/causes', name:"app_causes")]
    public function causes(Request $request){
        $count = count($this->causeRepository->findBy(['archive' => false]));
        $nombre_pages = $count % 3 == 0 ? intdiv($count, 3) : intdiv($count, 3) + 1;

        $page_actuel = isset($_GET['page']) ? $_GET['page'] : 1;

        $causesSet = $this->causeRepository->findBy(['archive' => false], [], 3, 3*($page_actuel -1));

        $causes = [];

        foreach ($causesSet as $cause) {
            $total = 0;
            foreach ($cause->getDons() as $don) {
                if ($don->getStatut() == "completed") $total += $don->getMontant();
            }

            $pourcentage = round(($total /$cause->getObjectifCause()), 2);

            $causes[] = [
                'id_cause' => $cause->getIdCause(),
                'nom_cause' => $cause->getNomCause(),
                'description_cause' => $cause->getDescripionCause(),
                'objectif_cause' => $cause->getObjectifCause(),
                'total_cause' => $total,
                'image_cause' => $cause->getImageCause(),
                'pourcentage' => $pourcentage
            ];
        }

        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_causes');
            }
        }

        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render('home_page/causes.html.twig', [
            'causes' => $causes,
            'total' => $count,
            'statut_page' => [
                'page_actuel' => $page_actuel,
                'nbre_pages' => $nombre_pages
            ],
            'newsletterForm' => $newsletterForm->createView(),
        ]);
    }

    #[Route('contact', name:"app_contact")]
    public function contact(Request $request){
        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_contact');
            }
        }
        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        $contactForm = $this->createForm(ContactFormType::class, null, [
            'action' => $this->generateUrl('app_handle_contact')
        ]);

        return $this->render("home_page/contact.html.twig", [
            'newsletterForm' => $newsletterForm->createView(),
            'contactForm' => $contactForm->createView()
        ]);
    }

    #[Route('compte', name:"app_compte")]
    public function compte(Request $request){
        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_compte');
            }
        }

        $donateurForm = $this->createForm(DonateurFormType::class, $this->getUser());
        $donateurForm->handleRequest($request);
        
        if ($donateurForm->isSubmitted() && $donateurForm->isValid()) { 
            $donateur = $this->getUser();

            $donateur->setNomDonateur($donateurForm->get('nom_donateur')->getData());
            $donateur->setPrenomDonateur($donateurForm->get('prenom_donateur')->getData());
            $donateur->setContactDonateur($donateurForm->get('contact_donateur')->getData());

            $this->entityManager->flush();

            return $this->redirectToRoute("app_compte");
        }

        $count = count($this->donRepository->findBy(['donateur' => $this->getUser()]));
        $nombre_pages = $count % 5 == 0 ? intdiv($count, 5) : intdiv($count, 5) + 1;
        
        $page_actuel = isset($_GET['page']) ? $_GET['page'] : 1;

        $donsSet = $this->donRepository->findBy(['donateur' => $this->getUser()], ['date_heure' => 'DESC'], 5, 5*($page_actuel -1));

        
        $dons = [];
        foreach ($donsSet as $don) {
            $dons[] = [
                'id_don' => $don->getIdDon(),
                'date_heure' => $don->getDateHeure(),
                'montant' => $don->getMontant(),
                'id_transaction' => $don->getIdTransaction(),
                'cause' => $don->getCause()->getNomCause(),
                'statut' => $don->getStatut()
            ];
        }
        
        $total = 0;
        foreach ($this->getUser()->getDons() as $don) {
            if ($don->getStatut() == "completed") $total += $don->getMontant();
        }
        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render("home_page/compte.html.twig", [
            'newsletterForm' => $newsletterForm->createView(),  
            'donateur' => [
                'nom' => $this->getUser()->getNomDonateur(),         
                'prenom' => $this->getUser()->getPrenomDonateur(),         
                'email' => $this->getUser()->getEmail(),         
                'contact' => $this->getUser()->getContactDonateur(),    
            ],
            'donateurForm' => $donateurForm->createView(),
            'dons' => $dons,
            'total' => $total,
            'statut_page' => [
                'page_actuel' => $page_actuel,
                'nbre_pages' => $nombre_pages
            ],
        ]);
    }

    #[Route('/handle/contact', name:'app_handle_contact')]
    public function handleContact(Request $request, MailerInterface $mailer){
        $contactForm = $this->createForm(ContactFormType::class);
        $contactForm->handleRequest($request);
        
        if ($contactForm->isSubmitted() && $contactForm->isValid()) { 

            foreach ($this->donateurRepository->findAll() as $donateur) {
                if($donateur->isAdmin()){
                    $mailer->send(
                        (new TemplatedEmail())
                            ->from(new Address('songre226@gmail.com', "Songré"))
                            ->to($donateur->getEmail())
                            ->subject($contactForm->get('sujet')->getData())
                            ->htmlTemplate('home_page/mailContact.html.twig')
                            ->context([
                                'message' => $contactForm->get('message')->getData()
                            ])
                    );
                }
            }

            return $this->redirectToRoute('app_handle_contact_success');

        }

        else{
            return $this->redirectToRoute('app_home_page');
        }
    }

    #[Route('handle/contact/success', name:'app_handle_contact_success')]
    public function handleContactSuccess(Request $request){
        $newsletterForm = $this->createForm(NewsletterFormType::class);
        $newsletterForm->handleRequest($request);
        
        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()) { 
            if(!$this->newsletterRepository->findOneBy(['email' => $newsletterForm->get('email')->getData()])){
                $newsletter = $newsletterForm->getData();

                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_contact');
            }
        }
        if (!$this->getUser()->isVerified()) return $this->render('registration/verification.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);

        return $this->render('home_page/handleContact.html.twig', [
            'newsletterForm' => $newsletterForm->createView()
        ]);
    }
}
