<?php

namespace App\Controller;

use App\Entity\Cause;
use App\Entity\Don;
use App\Entity\Donateur;
use App\Form\DonFormType;
use App\Repository\CauseRepository;
use App\Repository\DonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\TexterInterface;
use Symfony\Component\Routing\Annotation\Route;

class PaiementController extends AbstractController
{      
    private $entityManager;
    private $causeRepository;
    private $donRepository;

    public function __construct(EntityManagerInterface $entityManager, CauseRepository $causeRepository, DonRepository $donRepository)
    {
        $this->entityManager = $entityManager;
        $this->causeRepository = $causeRepository;
        $this->donRepository = $donRepository;
    }
    
   /**
    * Payin_with_redirection
    *
    * @param  mixed $donateur
    * @param  mixed $cause
    * @param  mixed $don
    * @return void
    */
   public function Payin_with_redirection(Don $don)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://app.ligdicash.com/pay/v01/redirect/checkout-invoice/create",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '
                              {
                              "commande": {
                                "invoice": {
                                  "items": [
                                    {
                                      "name": "' . $don->getCause()->getNomCause() . '",
                                      "description": "' . $don->getCause()->getDescripionCause() . '",
                                      "quantity": 1,
                                      "unit_price": "' . $don->getMontant() . '",
                                      "total_price": "' . $don->getMontant() . '"
                                    }
                                  ],
                                  "total_amount": "' . $don->getMontant() . '",
                                  "devise": "XOF",
                                  "description": "' . $don->getCause()->getNomCause() . '",
                                  "customer": "",
                                  "customer_firstname":"' . $don->getDonateur()->getPrenomDonateur() . '",
                                  "customer_lastname":"' . $don->getDonateur()->getNomDonateur() . '",
                                  "customer_email":"' . $don->getDonateur()->getEmail() . '"
                                },
                                "store": {
                                  "name": "Apps",
                                  "website_url": "https://nos3s.com"
                                },
                                "actions": {
                                  "cancel_url": "http://127.0.0.1:8000/don/' . $don->getIdDon() . '/annuler",
                                  "return_url": "http://127.0.0.1:8000/don/' . $don->getIdDon() . '/statut",
                                  "callback_url": "http://127.0.0.1:8000/don/' . $don->getIdDon() . '/statut"
                                },
                                "custom_data": {
                                  "transaction_id": "' . $don->getIdTransaction() . '"
                                }
                              }
                            }',
            CURLOPT_HTTPHEADER => array(
                "Apikey: SE4PCIJSPK9Q91FF7",
                "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZF9hcHAiOiIyNzUiLCJpZF9hYm9ubmUiOiI3MjI5IiwiZGF0ZWNyZWF0aW9uX2FwcCI6IjIwMjAtMDgtMjQgMTE6Mzk6MjAifQ.OWgt_Rtx5hTePYUoF4n5Gga90F1SVYvXrnlEJt3S-Uk",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = json_decode(curl_exec($curl));

        curl_close($curl);
        return $response;
    }
    
    /**
     * StatusPayin
     *
     * @param  mixed $don
     * @return void
     */
    public function StatusPayin(Don $don){
	
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://app.ligdicash.com/pay/v01/redirect/checkout-invoice/confirm/?invoiceToken=".$don->getTokenTransaction(),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Apikey: QJXKBZZRHHWYFIQ6L",
            "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZF9hcHAiOiI4NjAiLCJpZF9hYm9ubmUiOiIxMDM2MzUiLCJkYXRlY3JlYXRpb25fYXBwIjoiMjAyMS0wOS0yOCAyMzowNjoyMSJ9.RkIIGjQVBHQrtQ7qlN6puQ8lsA4ULbwiH01l8A3XPeM"
          ),
        ));
        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        
        return $response;
    }

    #[Route('/cause/{id_cause}/donner', name: 'app_cause_donner')]
    public function index(Request $request, string $id_cause): Response
    {
        $cause = $this->causeRepository->findOneBy(['id_cause' => $id_cause]);
        $donateur = $this->getUser();

        $donForm = $this->createForm(DonFormType::class);
        $donForm->handleRequest($request);
        
        if ($donForm->isSubmitted() && $donForm->isValid()) { 
            $newDon = $donForm->getData();

            $newDon->setCause($cause);
            $newDon->setDonateur($donateur);
            $newDon->setIdDon(uniqid());
            $newDon->setIdTransaction('LGD'.date('Y').date('m').date('d').'.'.date('h').date('m').'.C'.rand(5,100000));

            $paiement = $this->Payin_with_redirection($newDon);

            $newDon->setTokenTransaction($paiement->token);

            $this->entityManager->persist($newDon);
            $this->entityManager->flush();

            if (isset($paiement->response_code) && $paiement->response_code == "00"){
                return $this->redirect($paiement->response_text);
            }
        }
    }

    #[Route('/don/{id_don}/statut', name:'app_don_statut')]
    public function donStatut(string $id_don, TexterInterface $texter, MailerInterface $mailer){
        $don = $this->donRepository->findOneBy(['id_don' => $id_don]);

        $statut = $this->StatusPayin($don);

        if (isset($statut)){
            $don->setStatut(trim($statut->status));

            $this->entityManager->flush();
            $sms = new SmsMessage(
              // the phone number to send the SMS message to
              '+22665689156',
              // the message
              "L'utilisateur ". $this->getUser()->getPrenomDonateur() . " " . $this->getUser()->getNomDonateur() ." vient d'éffectuer un don de " . $don->getMontant() . "Fcfa sur la plateforme Songré.",
              // optionally, you can override default "from" defined in transports
              '+17076752139',
              // you can also add options object implementing MessageOptionsInterface
          );
  
           $sentMessage = $texter->send($sms);

           $email = (new TemplatedEmail())
              ->from(new Address('songre226@gmail.com', 'Songre'))
              ->to($this->getUser()->getEmail())
              ->subject("Félicitations pour votre don !")
              ->htmlTemplate('home_page/paiementEmail.html.twig')
              ->context([
                  'montant' => $don->getMontant(),
                  'cause' => $don->getCause()->getNomCause()
              ]);

          $mailer->send($email);
        }

        return $this->render('home_page/paiementStatut.html.twig', [
          'cause' => [
            'id_cause' => $don->getCause()->getIdCause(),
            'nom_cause' => $don->getCause()->getNomCause(),
          ],
          'newsletterForm' => null
        ]);
    }

    #[Route('/don/{id_don}/annuler', name:"app_don_annuler")]
    public function annulerDon(string $id_don){
      $don = $this->donRepository->findOneBy(['id_don' => $id_don]);

      $statut = $this->StatusPayin($don);

      if(isset($statut)){
        if (isset($statut)){
          $don->setStatut(trim($statut->status));

          $this->entityManager->flush();
      }

      return $this->redirectToRoute('app_cause_details', ['id_cause' => $don->getCause()->getIdCause()]);
      }
    }
}
