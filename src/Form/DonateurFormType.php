<?php

namespace App\Form;

use App\Entity\Donateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DonateurFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom_donateur', TextType::class, [
                'label' => "Votre nom",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add('prenom_donateur', TextType::class, [
                'label' => "Votre prénom",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add('contact_donateur', TextType::class, [
                'label' => "Votre contact",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Donateur::class,
        ]);
    }
}
