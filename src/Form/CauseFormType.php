<?php

namespace App\Form;

use App\Entity\Cause;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CauseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom_cause', TextType::class, [
                'label' => "Intitulé",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "Saisissez le nom"
                ],
            ])
            ->add('descripion_cause', TextareaType::class, [
                'label' => "Description",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "Veuillez entrez une description",
                    'rows' => "3"
                ],
            ])
            ->add('objectif_cause', IntegerType::class, [
                'label' => "Objectif",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "10000000"
                ],
            ])
            ->add('image_cause', FileType::class, [
                'label' => "Illustration",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'accept' => "image/*"
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cause::class,
        ]);
    }
}
