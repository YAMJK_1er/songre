<?php

namespace App\Form;

use App\Entity\Donateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', TextType::class, [
                'label' => "Email",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "exemple@exemple.com"
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => "J'accepte les conditions d'utilisation ",
                'label_attr' => [
                    'class' => "form-check-label"
                ],
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                "label" => "Mot de passe",
                'label_attr' => ['class' => "form-label"],
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password', 'class' => "form-control"],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit être d\'au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('nom_donateur', TextType::class, [
                'label' => "Nom",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "SAWADOGO"
                ]
            ])
            ->add('prenom_donateur', TextType::class, [
                'label' => "Prénom",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "Nadine"
                ]
            ])
            ->add('contact_donateur', TextType::class, [
                'label' => "Contact",
                'label_attr' => [
                    'class' => "form-label"
                ],
                'attr' => [
                    'class' => "form-control",
                    'placeholder' => "+22600000000"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Donateur::class,
        ]);
    }
}
