<?php

namespace App\Entity;

use App\Repository\CauseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CauseRepository::class)]
class Cause
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::GUID)]
    private ?string $id_cause = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $descripion_cause = null;

    #[ORM\Column(length: 255)]
    private ?string $nom_cause = null;

    #[ORM\Column(type: Types::BIGINT)]
    private ?string $objectif_cause = null;

    #[ORM\Column(length: 255)]
    private ?string $image_cause = null;

    #[ORM\OneToMany(mappedBy: 'cause', targetEntity: Don::class)]
    private Collection $dons;

    #[ORM\Column]
    private ?bool $archive = null;

    public function __construct()
    {
        $this->dons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCause(): ?string
    {
        return $this->id_cause;
    }

    public function setIdCause(string $id_cause): static
    {
        $this->id_cause = $id_cause;

        return $this;
    }

    public function getDescripionCause(): ?string
    {
        return $this->descripion_cause;
    }

    public function setDescripionCause(?string $descripion_cause): static
    {
        $this->descripion_cause = $descripion_cause;

        return $this;
    }

    public function getNomCause(): ?string
    {
        return $this->nom_cause;
    }

    public function setNomCause(string $nom_cause): static
    {
        $this->nom_cause = $nom_cause;

        return $this;
    }

    public function getObjectifCause(): ?string
    {
        return $this->objectif_cause;
    }

    public function setObjectifCause(string $objectif_cause): static
    {
        $this->objectif_cause = $objectif_cause;

        return $this;
    }

    public function getImageCause(): ?string
    {
        return $this->image_cause;
    }

    public function setImageCause(string $image_cause): static
    {
        $this->image_cause = $image_cause;

        return $this;
    }

    /**
     * @return Collection<int, Don>
     */
    public function getDons(): Collection
    {
        return $this->dons;
    }

    public function addDon(Don $don): static
    {
        if (!$this->dons->contains($don)) {
            $this->dons->add($don);
            $don->setCause($this);
        }

        return $this;
    }

    public function removeDon(Don $don): static
    {
        if ($this->dons->removeElement($don)) {
            // set the owning side to null (unless already changed)
            if ($don->getCause() === $this) {
                $don->setCause(null);
            }
        }

        return $this;
    }

    public function isArchive(): ?bool
    {
        return $this->archive;
    }

    public function setArchive(bool $archive): static
    {
        $this->archive = $archive;

        return $this;
    }
}
