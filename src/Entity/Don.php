<?php

namespace App\Entity;

use App\Repository\DonRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DonRepository::class)]
class Don
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::GUID)]
    private ?string $id_don = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date_heure = null;

    #[ORM\Column(type: Types::BIGINT)]
    private ?string $montant = null;

    #[ORM\ManyToOne(inversedBy: 'dons')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Donateur $donateur = null;

    #[ORM\ManyToOne(inversedBy: 'dons')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Cause $cause = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $statut = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $id_transaction = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $token_transaction = null;

    public function __construct()
    {
        $this->date_heure = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdDon(): ?string
    {
        return $this->id_don;
    }

    public function setIdDon(string $id_don): static
    {
        $this->id_don = $id_don;

        return $this;
    }

    public function getDateHeure(): ?\DateTimeInterface
    {
        return $this->date_heure;
    }

    public function setDateHeure(\DateTimeInterface $date_heure): static
    {
        $this->date_heure = $date_heure;

        return $this;
    }

    public function getMontant(): ?string
    {
        return $this->montant;
    }

    public function setMontant(string $montant): static
    {
        $this->montant = $montant;

        return $this;
    }

    public function getDonateur(): ?Donateur
    {
        return $this->donateur;
    }

    public function setDonateur(?Donateur $donateur): static
    {
        $this->donateur = $donateur;

        return $this;
    }

    public function getCause(): ?Cause
    {
        return $this->cause;
    }

    public function setCause(?Cause $cause): static
    {
        $this->cause = $cause;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): static
    {
        $this->statut = $statut;

        return $this;
    }

    public function getIdTransaction(): ?string
    {
        return $this->id_transaction;
    }

    public function setIdTransaction(string $id_transaction): static
    {
        $this->id_transaction = $id_transaction;

        return $this;
    }

    public function getTokenTransaction(): ?string
    {
        return $this->token_transaction;
    }

    public function setTokenTransaction(?string $token_transaction): static
    {
        $this->token_transaction = $token_transaction;

        return $this;
    }
}
