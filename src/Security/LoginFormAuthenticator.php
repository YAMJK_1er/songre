<?php

namespace App\Security;

use App\Entity\Donateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager,UrlGeneratorInterface $urlGenerator ,CsrfTokenManagerInterface $csrfTokenManager, UserPasswordHasherInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
    }
    
    /**
     * supports
     *
     * @param  mixed $request
     * @return bool
     */
    public function supports(Request $request): bool
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route') && $request->isMethod('POST');
    }
    
    /**
     * getCredentials
     *
     * @param  mixed $request
     * @return void
     */
    public function getCredentials(Request $request){
        $credentials = [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
            '_csrf_token' => $request->request->get('_csrf_token'),
        ];

        $request->getSession()->set(Security::LAST_USERNAME, $credentials['email']);
        
        return $credentials;
    }
    
    /**
     * getUser
     *
     * @param  mixed $credentials
     * @param  mixed $userProvider
     * @return void
     */
    public function getUser($credentials, UserProviderInterface $userProvider){
        $user = $this->entityManager->getRepository(Donateur::class)->findOneBy(['email' => $credentials['email']]) ?? throw new UserNotFoundException(sprintf('Compte introuvable : "%s"', $credentials['email']));

        return $user;
    }
    
    /**
     * checkCredentials
     *
     * @param  mixed $credentials
     * @param  mixed $donateur
     * @return void
     */
    public function checkCredentials($credentials, UserInterface $donateur){
        $token = new CsrfToken('authenticate', $credentials['_csrf_token']);

        if (!$this->csrfTokenManager->isTokenValid($token)) throw new InvalidCsrfTokenException();

        return $this->passwordEncoder->isPasswordValid($donateur, $credentials['password']);
    }
    
    /**
     * authenticate
     *
     * @param  mixed $request
     * @return Passport
     */
    public function authenticate(Request $request): Passport
    {
        $email = $request->request->get('email', '');

        $request->getSession()->set(Security::LAST_USERNAME, $email);

        return new Passport(
            new UserBadge($email),
            new PasswordCredentials($request->request->get('password', '')),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
                new RememberMeBadge(),
            ]
        );
    }
    
    /**
     * onAuthenticationSuccess
     *
     * @param  mixed $request
     * @param  mixed $token
     * @param  mixed $firewallName
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        // For example:
        return new RedirectResponse($this->urlGenerator->generate('app_home_page'));
        throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
    }
    
    /**
     * getLoginUrl
     *
     * @param  mixed $request
     * @return string
     */
    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
