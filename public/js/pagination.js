let precedent = document.getElementById('precedent')
let suivant = document.getElementById('suivant')
let page = document.getElementById('page')
let paginationForm = document.getElementById('paginationForm')

precedent &&  precedent.addEventListener('click', () => {
    if (page.value > 1){
        page.value--
        paginationForm.submit();
    }
})

suivant && suivant.addEventListener('click', () => {
    if (page.value < parseInt(page.getAttribute('max'))){
        page.value++
        paginationForm.submit();
    }
})